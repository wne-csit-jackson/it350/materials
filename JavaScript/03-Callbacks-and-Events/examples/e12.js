// Don't hog the processor.
// Do a little work, schedule self to do more work later.
// Allows other "processes" to run.
// Simulates asynchronous processes.

let a_count = 5;
function a() {
    if (a_count > 0) {
        a_count -= 1;
        console.log("A");
        setTimeout(a, 1000);    
    }
}

let b_count = 3;
function b() {
    if (b_count > 0) {
        b_count -= 1;
        console.log("B");
        setTimeout(b, 700);
    }
}

setTimeout(a, 1000);
setTimeout(b, 1000);

console.log("Done running mainline");