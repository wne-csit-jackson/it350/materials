# Homework

1. Watch "What the heck is the event loop anyway?" https://youtu.be/8aGhZQkoFbQ
   and write a short (250-500 words) summary containing the main points.

2. Pick a JavaScript topic that interests you. Find and consume a few sources
   on the topic and write a short (250-500 words) summary containing the main
   points. Sources can be any publicly web accessible material (blog post,
   documentation, video, audio, etc.) or via O'Reilly Online (since we all have
   access to that). Cite your references in any standard format; ensure a link
   is included.

