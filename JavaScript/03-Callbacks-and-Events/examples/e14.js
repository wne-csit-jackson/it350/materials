// Example based on 13.1.4 in JavaScript the Definitive Guide
const fs = require("fs");
let options = {};

function startProgram(options) {
    console.dir(options);
}

fs.readFile("examples/config.json", "utf-8", (err, text) => {
    if (err) {
        console.warn("Could not read config.json");
    } else {
        Object.assign(options, JSON.parse(text));
    }
    startProgram(options);
});

console.log("This prints first, why?");