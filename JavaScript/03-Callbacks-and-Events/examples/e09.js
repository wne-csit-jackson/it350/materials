// The passed function can take parameters too.

function filter_items(items, is_too_big_function) {
    keep = [];
    for (i in items) {
        if ( ! is_too_big_function(items[i])) {
            keep.push(items[i]);
        }
    }
    return keep;
}

let kept = filter_items([42, 12, 8, 23], x => x > 15);
console.log(kept);