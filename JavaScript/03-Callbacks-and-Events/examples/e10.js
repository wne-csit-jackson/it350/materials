// The callback pattern.

function read_file(filename, failure_callback, success_callback) {
    let contents = null;
    try {
        // Uncomment the following line to simulate a failure during read.
        // throw "No such file.";

        // Pretend that we read the file into contents.
        contents = "This is the contents of the file.";
    } catch (e) {
        failure_callback(e);
    }
    success_callback(contents);
}


read_file("docker-compose.yml", e => console.dir(e), contents => {
    console.log(contents);
});
