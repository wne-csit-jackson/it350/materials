# JavaScript: Use Third Party Libraries

> NOTE: On Windows, use command prompt to run any interactive Docker or Docker Compose commands.

## Model 1 (5 min)

1. Clone the project this activity is in and change into the directory containing this Activity.md file.

2. Inspect docker-compose.yml and hello.js. What's new since the last activity?

3. Inspect .gitignore and data/.gitignore. What are their purposes?

## Model 2 (5 min)

- Yarn is a package manager for JavaScript and Node.
- We'll use it to install 3rd party libraries for Node.

1. Add the following service to docker-compose.yml

    ```yaml
      yarn:
        image: "node:14.13"
        volumes:
          - type: "bind"
            source: "."
            target: "/workdir"
        working_dir: "/workdir"
        entrypoint: [ "yarn" ]
    ```

- Note the use of `entrypoint`. This defines the base instruction to run when
   this service is ran. Any arguments provided on the commandline to the service
   will be appended to this base command. We'll use this to provide subcommands
   to `yarn` when we run this service.

## Model 2 (5 min)

- Each project has a different set of dependencies.
- Yarn manages each project's dependences separately.
- Before we can use yarn, we must initialize Yarn for our project.

1. Run and enter the values given below. Yarn will ask you several
   questions about your project, enter the values give below.
   Where no value is given, just press enter.

    ```
    docker-compose run --rm yarn init
    ...
    question name (workdir): hello
    question version (1.0.0): 
    question description: Mongo db example.
    question entry point (index.js): hello.js
    question repository url: 
    question author: INITIALS OF TEAM MEMBERS
    question license (MIT): GPL-v3.0
    question private: true
    success Saved package.json
    Done in 164.88s.
    ```

2. What new file was created? Inspect its contents.


## Model 3 (10 min)

- MongoDB provides "drivers" for different programming languages.
- A MongoDB driver is an API (application programmers interface)
  for programmatically interacting with a MongoDB server.
- Starting from MongoDB's website (<https://www.mongodb.com/>) one can
  find documentation for drivers, and we want the NodeJS driver.
  Under their "Quickstart" instructions, we see that their published
  JavaScript package name is `mongodb` (that's what they use when installing
  the driver using `npm` an alternative package manager, similar to `yarn`).
- Let's use `yarn` to add `mongodb` as a dependency for our project.

1. Run
    ```
    docker-compose run --rm yarn add mongodb
    ```

2. What new directory did the command above create?
   Approximately how many subdirectories does it contain?
   What do you think each of those directories contain or represent?

3. What new file did the command above create? Inspect its contents.

4. Inspect `package.json`. What was added?

5. Why do you think are there so many more dependencies in `node_modules` and `yarn.lock`
   than there are dependencies listed in `package.json`?

6. What do you think is the purpose of `yarn.lock`?

7. Stage and commit `package.json` and `yarn.lock` to your repository.


## Model 4 (15 min)

- We're ready to use mongodb in our implementation of `hello.js`.

1. Replace the contents of `hello.js` with the following contents.

```
const { MongoClient } = require("mongodb");

// Replace string with your MongoDB's connection string.
const uri = "mongodb://mongo/";
const client = new MongoClient(uri, { useUnifiedTopology: true });

async function run() {
  try {
    await client.connect();

    const database = client.db("sample_mflix");
    const collection = database.collection("movies");

    // create a document to be inserted
    const doc = { title: "Tron", director: "Steven Lisberger" };
    const result = await collection.insertOne(doc);

    console.log(
      `${result.insertedCount} documents were inserted with the _id: ${result.insertedId}`,
    );
  
    // Find all movies and display them.
    const movies = await collection.find().toArray();
    movies.forEach(movie => console.log(movie));
  
  } finally {
    await client.close();
  }
}

// Returns a promise that resolves after ms time.
function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Call run after 3000ms.
// If anything goes wrong call console.dir() to display some debug information.
delay(3000).then(run).catch(console.dir)
```

2. Run the system.
    ```
    docker-compose up --detach
    ```

3. After a few seconds, check the logs.
    ```
    docker-compose logs
    ```

4. Look for lines from the `hello` service. They should look something like the following.
    ```
    hello_1  | 1 documents were inserted with the _id: 5f863922def47900cddacefb
    hello_1  | {
    hello_1  |   _id: 5f863922def47900cddacefb,
    hello_1  |   title: 'Tron',
    hello_1  |   director: 'Steven Lisberger'
    hello_1  | }
    ```

5. If you don't see such lines, try running `docker-compose logs` again
   (the processes may not have settled down by the time you first ran the command),
   or try stopping the sytsem `docker-compose down`, and then retry steps 2-4.

6. Try stopping and reruning the system. What's changed about `hello`'s output?

7. Modify hello.js to insert a different movie that also contains a `publisher` field (e.g., Disney).
   Test it by stopping and restarting the system.

8. At this point your team should have many questions, list some of them here.


---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
