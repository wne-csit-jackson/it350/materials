// Pass a different function. Notice greet didn't change since previous example.

function say_hi() {
    console.log("hi");
}

function say_hello() {
    console.log("hello");
}

function greet(greeting_function) {
    greeting_function();
}

greet(say_hello);