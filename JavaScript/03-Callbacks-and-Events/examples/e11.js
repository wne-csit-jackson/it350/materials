// Schedule methods to run later.

setTimeout(() => {
    console.log("A");
}, 500);

setTimeout(() => {
    console.log("B");
}, 1000);

setTimeout(() => {
    console.log("C");
}, 0);

console.log("Done running mainline");

// Notice that even though C is schedule for 0ms delay, it still comes after the last line runs.
// JavaScript is single threaded, and scheduled code won't have a chance to run until the
// current executing code is done.