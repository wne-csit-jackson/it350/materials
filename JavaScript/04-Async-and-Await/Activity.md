# Promises, async/await, and exception handling exploration

Form teams with the roles: (two-person teams combine copilot and navigator)

* pilot: manipulates code and tools
* copilot: assists pilot with technical challenges
* navigator: helps to identify the direction the team should go in

Use the code in materials/JavaScript/04-Async-and-Await in
<https://gitlab.com/wne-csit-jackson/it350/materials>
to construct experiments to answer the questions in the "Questions" section below.
Do this by,

1. Claiming a question for your team by putting the initials of your team members below the question.
2. Constructing a minimal experiment-example that demonstrates the answer. Use comments to breifly explain the answer.
3. Paste your answer under the question.

* If you come up with a new question, add it to the list (if it hasn't been asked).
* If you give up remove your claim from the question.

## Questions

1. Could you explain the exception handling in class?

2. What happens when a promise object that returns is rejected?

3. Does the await keyword mean that the program will wait until the promise is settled before continuing to the next line?

4. Are you able to use multiple catches for async functions?

5. I know we can use await when calling any function that returns a promise. However, does that include web API functions?

6. So async can be used without await, but await can only be used with async right?

7. What are the different types of exceptions?

8. How do people handle codes if there are not async and await?

9. If I executed an asynchronous method but didn’t  await it would that be a problem?
