// Static scoping: functions can refer to variables that are in scope
// where the function is defined.

function greet(greeting_function) {
    greeting_function();
}

let message = "Hello";
greet(() => {
    console.log(message);
});
