// Static scope: ... and they can modify those variables.


function greet(greeting_function) {
    greeting_function();
}


function main() {
    let message = "Hello";

    greet(() => {
        message += ", World."
        console.log(message);
    });

    console.log(message);
}


main();
