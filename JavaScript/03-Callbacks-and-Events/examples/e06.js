// Don't forget functions can have local variables.

function greet(greeting_function) {
    greeting_function();
}

greet(() => {
    let message = "Hello";
    console.log(message);
});
