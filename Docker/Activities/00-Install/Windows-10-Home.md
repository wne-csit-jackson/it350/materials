# Docker Desktop on Windows 10 Home

This snipet was written on 9/3/2020. It was last reviewed on 9/3/2020.

If you want to build Linux-based Docker containers using a computer running Windows 10 Home, this guide is for you.

If you are running a different flavour of Windows, this guide might give you some clues as to how to install and run Docker Desktop on your system, but the author has not confirmed its usefulness for other flavours of Windows.

A month before this document was written, Microsoft released version 2004 of Windows 10 Home. The release supports the new Windows Subsystem for Linux version 2 (WSL 2), which is needed to install and run Docker for Desktop. The install instructions below will point you to resources for enabling WSL 2.

To use WSL, your computer's hardware must support virtualization, and it must be enabled. The install instructions below will point you to resources for enabling virtualization in your system's BIOS.

If your hardward does not support virtualization, or you cannot enable virtualization, then you will not be able to install Docker for Desktop. However, you can still run Docker by installing Docker Toolbox. This used to be the only way to use Docker on Windows 10 Home. It is not as effecient as Docker for Desktop, and requires you to install and interact with VirtualBox, which Docker Toolkit uses to manage a Linux virtual machine in which your containers will run. For more information about **Docker Toolbox** see <https://docs.docker.com/toolbox/toolbox_install_windows/> .

## Installing Docker for Desktop

Complete installation instructions for all operating systems can be found here:
https://docs.docker.com/get-docker/ .

1. Enable virtualization in your BIOS.

    1. Check if virtualization is enabled: <https://thegeekpage.com/how-to-check-if-virtualization-is-enabled-in-windows-10/>

    2. Enable virtualization in BIOS: <https://2nwiki.2n.cz/pages/viewpage.action?pageId=75202968>

    3. Confirm it is anabled by repeating step 1.

    4. If you cannot enable virtualization in your BIOS, you will need to use **Docker Toolbox** instead &mdash; see <https://docs.docker.com/toolbox/toolbox_install_windows/> .

2. Update to Windows 10, version 2004 or higher.

    1. Check your Windows' version: <https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running>

    2. Use Windows Updater to install all updates available for your system.

    3. Confirm your Windows' version is 2004 by repeating step 1.

    4. If you still don't have version 2004 or higher Use Microsoft Update Assistant: <https://support.microsoft.com/en-us/help/3159635/windows-10-update-assistant> to manually install the latest version of Windows 10 Home. Then confirm our Windows' version is 2004.

3. Install and enable WSL 2: <https://docs.microsoft.com/en-us/windows/wsl/install-win10>.

    1. In the instructions linked above, if you get an error when you run `wsl --set-default-version 2` in the previous step, update the wsl kernel: <https://aka.ms/wsl2kernel> . Then try `wsl --set-default-version 2` again.

4. Install Docker Desktop for Windows 10 Home: <https://docs.docker.com/docker-for-windows/install-windows-home/>

## Using Docker Desktop

### Check if Docker Desktop is running

Look for the Docker icon in your system tray. If it is there, Docker Desktop is running.

### Start Docker Desktop

Use your system's Start menu to search for and run Docker Desktop. It takes some time for Docker Desktop to initialize. The Docker icon in your system tray will be animated while it is initializing. Docker Desktop is ready when the icon stops animating.

### Stop Docker Desktop

To stop Docker Desktop, click the Docker icon in the system tray and select `Quit Docker Desktop`.

### Configuring Docker Desktop

Click the Docker icon in your system tray and select `Dashboard`. Once the Dashboard is running, click its gear icon.

Among other configurations, unless you regularly use Docker, you may want to disable `Start Docker when you log in`. Of course, if you disable this, you will need to remember to start Docker Desktop when you want to use Docker.

## Using Docker

Below you'll find a few tips for using Docker on Windows.
This is not a complete guide to Docker. For that, see the Docker's documentation: <https://docs.docker.com/>

### Ensure that Docker Desktop is running

Before you can use Docker, start Docker Desktop if it is not currently running. See above.

### Use Docker and Docker Compose

`docker` and `docker-compose` must be run from the command-line. Use either the Command Prompt or PowerShell. To start either, use your system's Start menu to search for and run them. Both `docker` and `docker-compose` were added to your system path with you installed Docker Desktop. So the following should work.

```
docker --version
docker-compose --version
```

### Mounting the current directory into a container

Often we want the files in our current directory to be available inside the container. Depending on whether you are using a Command Prompt, or PowerShell, Use either `%cd%` or `${PWD}` to get the full path to the current working directory. The example in the table below mounts the current working director at the location `/some/dir` within the container.

Shell | Variable | Example
----- | -------- | -------
Command Prompt | `%cd%` | `docker run -v "%cd%:/some/dir" my-container`
PowerShell | `${PWD}` | `docker run -v "${PWD}:/some/dir" my-container`

In general, when specifying a path to Docker for a file or directory on your host machine (Windows), you can use either forwardslashes or backslashes. For example, the following are equivalent.

- `C:\Users\Me`
- `C:/Users/Me`

When specifying a path to Docker for a file that is ***inside*** a container (Linux), you must use Linux style paths &mdash; so forwardslashes.

- `/home/me`

---
Copyright 2020, Stoney Jackson <dr.stoney@gmail.com>

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
