# Homework 4 &mdash; REST API Design

## Part 1 &mdash; list canceled orders

The current API allows orders to be canceled, but does not provide a way to list the orders that have been canceled.

* Design a new endpoint that lists canceled orders.
* Update `OrderModule/openapi.yml` to specify your new endpoint.
* The endpoint should belong to the appropriate existing service.
* Place your specification in an appropriate place within `OrderModule/openapi.yml`.
* Appropriate parameters must be specified.
* Your design must take into consideration that over time the number of canceled orders will grow over time, and your endpoint should help ensure predictable performance.
* Appropriate responses must be specified.
* Follow conventions established in other parts of the specification.
* Regularly test your specification regularly by running `make down build up` inside `shell/run.sh`

## Part 2 &mdash;

Design and specify a new REST API endpoint to add a new item to the items collection.

- Edit the `OrderModule/openapi.yml` file in your cloned copy of the `microservices-example` project to add the new endpoint.
- Be sure you place it in an appropriate place in the file.
- You must include all the necessary parts of the specification.
- Build the documentation and check the web page to see if it looks the way you want.

## Deliverables

* Modified `openapi.yml` file pushed to your fork of the microservices-example project.


## General Technical Instructions

### First-time setup

1. Fork **microservices-example** into your individual subgroup of IT 350 on GitLab.
    * microservices-example: <https://gitlab.com/LibreFoodPantry/training/microservices-example>
    * your subgroup of IT 350 (replace LASTNAME with your lastname): <https://gitlab.com/wne-csit-jackson/it350/individual/LASTNAME>
2. Clone ***your fork*** to your computer.


### Each work session setup

With Docker running, in bash (git-bash), in the root of the project you cloned, start the development shell

```bash
shell/run.sh
```

### Viewing/testing your API specification during a work session

Within the development shell, stop the services, rebuild the services, and then restart the services.

```bash
make down build up
```

Now open your browser to <http://localhost:10001/api-doc/>

Or if that URL is already opened in your browser, perform a *hard refresh* in your browser.


### Each work session teardown

In the development shell, stop containers.

```bash
make down
```

> NOTE: If your development shell is no longer running, you can start another (see previous section) and then stop your containers.

Exit your development shell.

```bash
exit
```

From the root of your project (which you are probably in from the last command), stage, commit, and push your changes.

```bash
git add .
git commit -m "Descriptive message about the state of your work"
git push
```

Confirm that your commit made exists in the correct location on GitLab by visiting <https://gitlab.com/wne-csit-jackson/it350/individual/LASTNAME/microservices-example> (replace LASTNAME with your lastname).

### Total teardown

First, perform the work session teardown if you have not already done so.

The following removes ***ALL*** Docker artificats (e.g., containers, images, networks, and volumes) from your system.

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker system prune -a -f --volumes
docker rmi $(docker images -a -q)
```

---

&copy; 2020 Karl R. Wurst and Stoney Jackson

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
