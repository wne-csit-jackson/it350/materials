
## Text

```
JavaScript: The Definitive Guide, 7th Edition
by David Flanagan
Publisher: O'Reilly Media, Inc.
Release Date: May 2020
ISBN: 9781491952023
```

Available on learning.oreilly.com for free via an ACM membership (student $19/yr).

## Schedule

| Reading                    | Activity
| -------------------------- | ---------------------------------- 
| Chapter 1 to 1.2 inclusive | 01-Run-JavaScript-in-Docker
| Chapter 1 remaining        | 01-Run-JavaScript-in-Docker (continued)
| Chapter 10.2               | 02-Use-Third-Party-Libraries
| Chapter 13 through 13.1 inclusive | 03-Callbacks-and-Events (2x 50m sessions)
| Chapter 13.2               | Go through examples in class (3-4x 50m sessions)
| Chapter 13.3               | 04-Async-and-Await (2x 50m session?)

