// Define and pass anonymous functions.

function greet(greeting_function) {
    greeting_function();
}

greet(function () {
    console.log("Hi");
});

greet(() => {
    console.log("Hello");
});
