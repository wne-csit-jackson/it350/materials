# SemVer - Semantic Versioning

## Model 1

1. Read the Summary on this page: <https://semver.org/>

2. Your web system using MongoDB. A new release of MongoDB has just come out. Assume MongoDB uses semantic versioning (they actually don't; but it is very similar) Using only the version number, assess the risk -- low, medium, or high -- that upgrading to the new version will break your system. Very briefly explain why in terms of SemVer.

| Current Version | New Version | Risk | Why?
| --------------- | ----------- | ---- | -----
| 2.3.1           | 3.0.0       |      |
| 2.3.1           | 2.4.0       |      |
| 2.3.1           | 2.3.2       |      |

3. You are about to cut a new release of your system. Review the changes that have been made since your last release and determine the new version on your system.

| Current Version | Changes                                           | New Version
| --------------- | ------------------------------------------------- | -----------
| 4.5.9           | Adjust email validator to handle sub domains.     |
| 4.5.9           | Changed the date format expected in config file.  |
| 4.5.9           | Added a new statistics report.                    |
| 4.5.9           | All of the above.                                 |
| 4.5.9           | 5 fixes and 1 new feature                         |

## Model 2

Most projects, when they cut a new release and produce a new Docker image, they assign the new image multiple tags. For example, suppose version 3.1.2 of widget is released. Then they assing the new image the following tags:

```
After the first release

latest
1
1.0
1.0.0
image1

After the second release - a patch release

        latest
        1
        1.0
1.0.0   1.0.1
image1  image2

After the third release - a patch release

                latest
                1
                1.0
1.0.0   1.0.1   1.0.2
image1  image2  image3

After fourth release - a minor release

                        latest
                        1
                1.0     1.1
1.0.0   1.0.1   1.0.2   1.1.0
image1  image2  image3  image4
```

1. At a particular moment in time, can images have more than one tag?

2. At a particular moment in time, can multiple images have the same tag?

3. When does the `latest` tag move to a new release?

4. When does a three component tag (e.g., 2.3.4) move to a new image?

5. When does a two component tag (e.g., 2.3) ***NOT*** move to a new image?

6. When do you think a one component tag (e.g., 2) will  ***NOT*** move to a new image?

7. Update the diagram below for a major release.

```
After the fifth release - a major release

                        latest
                        1
                1.0     1.1
1.0.0   1.0.1   1.0.2   1.1.0
image1  image2  image3  image4  image5
```

8. Suppose after the first release, you start using widget:1.0 like so

    ```
    docker pull widget:1.0
    docker run --name my-widget --detach widget:1.0
    ```

    What image are you running?

9. Now after the second release, you do the following...

    ```
    docker pull widget:1.0
    docker stop my-widget
    docker rm my-widget
    docker run my-widget
    ```

    What image do you think you are running now?

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.