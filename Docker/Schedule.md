
## Text

```
Docker in Action, Second Edition
by Stephen Kuenzli; Jeffrey Nickoloff
Published by Manning Publications, 2019
```

Available on learning.oreilly.com for free via an ACM membership (student $19/yr).

## Schedule

| Reading                  | Activity
| ------------------------ | ---------------------------------- 
|                          | 00-Install
| Chapter 1 - all          | 01-Advantages-Disadvantages
| Chapter 2 - stop at 2.3  | 02-Managing-Images-and-Containers
| Chatper 3 - all          | 03-Semantic-Versioning
| Chapter 4 - stop at 4.3  | 04-Bind-Mounting
| Chapter 5 - 5.1-5.2, 5.4 | 05-NodePort-Publishing
| [Get started with Docker Compose](https://docs.docker.com/compose/gettingstarted/) | 06a-Get-Started-With-Docker-Compose-Walkthrough
| [Compose File Reference](https://docs.docker.com/compose/compose-file/) | 06b-Writing-Docker-Compose-Files
| Video: [Docker for Beginners](https://youtu.be/i7ABlHngi1Q) <br> Click "show more" to see complete index. <br> Watch 10:00 to 24:35 | 07-Building-Docker-Images

