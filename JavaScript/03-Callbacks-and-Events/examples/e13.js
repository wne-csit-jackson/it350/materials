// Example based on 13.1.3 in JavaScript the Definitive Guide.
// Modified to use a real live service.

const { XMLHttpRequest } = require("xmlhttprequest");

function getPosts(postsCallback) {
    let request = new XMLHttpRequest();
    request.open("GET", "https://gorest.co.in/public-api/posts");
    request.send(); // This is asynchronous... no transmission until current "process" completes.

    // Callbacks may be given to designated fields of an object.
    // The onload field represents an event. If that event occurs, this callback is called.
    request.onload = function() {
        if (request.status === 200) {
            let postsText = request.responseText;
            postsCallback(null, postsText);
        } else {
            postsCallback(request.status, null);
        }
    };

    // onerror is called if there is an error; ontimeout is called if the request gives up.
    request.onerror = request.ontimeout = function(e) {
        postsCallback(e.type, null);
    };
}

getPosts((err, version) => {
    if (err) {
        console.error(err);
    } else {
        console.log("Success: " + version);
    }
});