// Pass the function to call to another. The other calls it.

function say_hi() {
    console.log("hi");
}

function greet(greeting_function) {
    greeting_function();
}

greet(say_hi);