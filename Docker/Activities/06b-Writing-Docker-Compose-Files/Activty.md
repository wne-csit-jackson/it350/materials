# Activity - Docker Compose File

To ensure Docker is working correctly, run the following in the root of this
actity (same directory with redis.conf).

```
docker run \
  --rm \
  --detach \
  --name cache \
  -p 11000:6379 \
  --mount type=bind,src="${PWD}/redis.conf",dst="/etc/redis.conf",readonly \
  redis:6.0.8 \
  /etc/redis.conf
```

Now stop and remove it.

Now complete docker-compose.yml to best approximate the above command so that
the following command is reasonably equivalent to the above.

```
docker-compose up --detach
```

Use https://docs.docker.com/compose/compose-file/ and any other source you like
to figure out how to best approximate the above configuration.

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
