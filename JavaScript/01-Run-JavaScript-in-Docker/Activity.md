# JavaScript: Run JavaScript in Docker

> NOTE: On Windows, use command prompt, and replace $PWD with %cd% in all commands.

## Model 1

1. Run the following command

    ```
    docker run -it --rm node
    ```

2. What output did you get?

3. Type the following JavaScript statement.

    ```
    console.log("Hello");
    ```

4. What output did you get? Try to explain your output.

5. Enter `.help` and figure out how to end your NodeJS session.

6. End your session.

7. What did you type to end your session?

8. What do you now know how to do?


## Model 2

1. Run the following command.

    ```
    docker run -it --rm node /bin/bash
    ```

    If successful you should be at a prompt that looks something like:

    ```
    root@06a6282f4276:/# 
    ```

2. Try to explain where and what you are running.


3. Enter `node`.


4. Try to explain where you are now.


5. Run a JavaScript statement.


6. Exit the session.


7. Exit the container.

8. What do you now know how to do?


## Model 3

1. Run bash in a NodeJS container.

2. Run the following commands

    ```
    echo 'console.log("Hi");' > hello.js
    node hello.js
    ```

3. Explain what you did and what happened.

4. Exit the container.

5. What do you now know how to do?


## Model 4

1. Create a plain text file named hello.js with the following contents.
   (If you cloned this project, hello.js should be in the same directory
   as this file.)

    ```
    console.log("Hi");
    ```

2. In the same directory as the file you just created, run the following

    ```
    docker run -it --rm --mount type=bind,src="$PWD",dst=/workdir node /bin/bash    
    ```

3. Inspect the contents of `/workdir`

    ```
    ls /workdir
    ```

4. Use node to run `/workdir/hello.js`.

5. What command(s) did you use in the previous step?

6. ***Without*** exiting the container,
   using an editor on your host machine (e.g., VS Code) to
    1. edit `hello.js`,
    2. change `Hi` to `Hello`, and
    3. save it.

7. In the container run `/workdir/hello.js` again.

8. Did it print `Hi` or `Hello`? What does this tell you?

9. Exit your the container.

10. What do you now know how to do?


## Model 5

1. Create a docker-compose.yml file with the following contents.
   (If you cloned this project, docker-compose.yml should be in the same directory
   as this file.)

    ```
    version: "3.8"
    services:
      hello:
        image: "node"
        volumes:
        - type: "bind"
          source: "."
          target: "/workdir"
        command: [ "/bin/bash" ]
    ```

    >! NOTE: Indentation matters in YAML.

2. In the directory containing the file you just created, run the following

    ```
    docker-compose run --rm hello
    ```

3. Where are you?

4. Run your `hello.js` script.

5. Exit the container.

6. What do you now know how to do?


## Model 6

1. Modify `command` in docker-compose.yml so it appears as follows.

    ```
    command: [ "node", "/workdir/hello.js" ]
    ```

2. Run your node service as before.

3. Explain your results.


## Model 7

1. To your hello service in docker-compose.yml
    1. Add the following `working_dir` value:
        ```
        working_dir: "/workdir"
        ```
    2. Modify the `command` value as follows:
        ```
        command: [ "node", "hello.js" ]
        ```
2. Test your service.

3. Explain the changes you made.


## Model 8

1. Future proof your hello service by pinning the node image to the current MAJOR.MINOR version but allow for new patch releases.

2. Test that it still works.

3. Paste your finale docker-compose.yml file here.

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
