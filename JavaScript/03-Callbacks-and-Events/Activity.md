# Activity - Callbacks and Events in Node

## Prerequisites

Docker must be installed and running.

## Setup

Install node modules.

```
docker-compose run yarn install
```

## Running the examples

Each example can be ran as follows.

```
docker-compose run node examples/FILE.js
```

For example, to run examples/e01.js

```
docker-compose run node examples/e01.js
```

## Instructions

For each example do the following.

1. Inspect the code.
2. Predict its output/behavior.
3. Run the code.
4. Reflect on what was you learned.
